import pygame as pg
from pygame.locals import *
import sys
import random
from typing import Dict
import math
from typing import Counter

# MODEL PARAMS

profits_to_rent = 30
delivery = True

# SIMULATION PARAMS
random.seed(42)
rent = 100  # paid every tick
base_capital = rent * 200
max_cash = base_capital * 4
profit = rent//profits_to_rent  # per customer
shop_chance_per_tick = 0.15
x_max = 1000
y_max = 1000
epochs = 10
# customer_groups = [
#     {
#         'rect_northwest': (50,50),
#         'rect_southeast': (550, 550),
#         'color': Color('black'),
#         'count': 250,
#         'distance_threshold_min': 20,  # used only if delivery = True
#         'distance_threshold_max': 50
#     },
#     {
#         'rect_northwest': (200,200),
#         'rect_southeast': (300, 300),
#         'color': Color('indianred'),
#         'count': 130,
#         'distance_threshold_min': y_max+x_max,
#         'distance_threshold_max': y_max+x_max
#     },
#     {
#         'rect_northwest': (400, 300),
#         'rect_southeast': (420, 320),
#         'color': Color(0,0,255),
#         'count': 10,
#         'distance_threshold_min': 20,  # used only if delivery = True
#         'distance_threshold_max': 50
#     },
# ]

customer_groups = [
    {
        'rect_northwest': (0,0),
        'rect_southeast': (1000, 1000),
        'color': Color('black'),
        'count': 500,
        'distance_threshold_min': 25,
        'distance_threshold_max': 150
    },
    {
        'rect_northwest': (200,200),
        'rect_southeast': (800, 800),
        'color': Color('indianred'),
        'count': 200,
        'distance_threshold_min': 15,
        'distance_threshold_max': 75
    },
    {
        'rect_northwest': (400, 400),
        'rect_southeast': (600, 600),
        'color': Color('darkgreen'),
        'count': 200,
        'distance_threshold_min': 20,
        'distance_threshold_max': 50
    }
]


# IMPLEMENTATION DETAILS CONSTANTS
LIMITTPS = False
RENDER_EVERY = 15
TPS = 60
SHOP_SIZE = 2
SHOP_COLOR = Color('steelblue')
FULLCASH_SHOP_COLOR = Color('cornflowerblue')
CUSTOMER_SIZE = 3
REPORTPROFIT_EVERY = 300

def setup():
    pg.init()
    screen = pg.display.get_surface()

    screen = pg.display.set_mode((x_max, y_max))
    if delivery:
        pg.display.set_caption("model-delivery-"+str(profits_to_rent))
    else:
        pg.display.set_caption("model-"+str(profits_to_rent))

    background = pg.Surface(screen.get_size())
    background = background.convert()
    background.fill((255, 255, 255))

    screen.blit(background, (0, 0))
    pg.display.flip()

    shops = pg.sprite.Group()

    customers = pg.sprite.Group()
    for group in customer_groups:
        for _ in range(group['count']):
            customers.add(Customer(group))

    clock = pg.time.Clock()
    counter = Counter()
    return shops, customers, screen, clock, background, counter

def main():
    shops, customers, screen, clock, background, counter = setup()
    simulate(shops, customers, screen, clock, background, counter, delivery)

def simulate(shops, customers, screen, clock, background, counter, deliv):
    delivery = deliv
    ticks = 0
    shop_location_changed = True
    shop_chance = shop_chance_per_tick
    epoch = 0
    while True:
        for event in pg.event.get():
            if event.type == QUIT:
                pg.quit()
                sys.exit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    pg.quit()
                    sys.exit()
        
        if random.random() <= shop_chance:
            shop_location_changed = True
            newshop = Shop(base_capital, rent, random.randint(0, x_max), random.randint(0, y_max), counter)
            shops.add(newshop)

        
        for c in customers.sprites():
            if shop_location_changed:
                c.closest = None
                c.closest_dist = math.inf
            for shop in shops.sprites():
                if shop_location_changed:
                    dist = distance(shop, c)
                    if dist < c.closest_dist:
                        c.closest = shop
                        c.closest_dist = dist
            if c.closest:
                c.consider_shop(c.closest, c.closest_dist)
        shop_location_changed = False


        if(ticks > REPORTPROFIT_EVERY*TPS):
            epoch += 1
            ticks = 0
            excessprofit = str(counter['p'] * profit)
            shopcount = str(len(shops))
            deliv = ""
            if delivery:
                deliv = "delivery-"
            pg.image.save(screen,"model-"+deliv+str(profits_to_rent)+"-"+str(epoch)+"::"+shopcount+"-"+excessprofit+".tga")
            # ls -1 *.tga | parallel convert '{}' '{.}.png'
            if epoch > 10:
                pg.quit()
                sys.exit()

            counter['p'] = 0
            shop_chance /= 1.4

        s = len(shops)
        shops.update()
        if s != len(shops):
            shop_location_changed = True
        if ticks % 2**8 == 0:
            screen.blit(background, (0, 0))
            shops.draw(screen)
            customers.draw(screen)
            pg.display.flip()  # possible optimization: .update()

        ticks += 1
        if LIMITTPS:
            clock.tick(TPS)


class Shop(pg.sprite.Sprite):
    def __init__(self, cash: int, rent: int, x: int, y: int, counter: Counter):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface([SHOP_SIZE, SHOP_SIZE])
        self.image.fill(SHOP_COLOR)

        self.cash = cash
        self.rent = rent

        self.closest = None
        self.closest_dist = math.inf

        self.counter = counter

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def update(self):  # called every tick
        self.cash -= self.rent
        if(self.cash <= self.rent):  # wouldnt make next rent
            self.kill()
        multiplier = self.cash/base_capital
        self.image = pg.Surface([SHOP_SIZE*multiplier, SHOP_SIZE*multiplier])
        if (self.cash > (0.9*max_cash)):
            self.image.fill(FULLCASH_SHOP_COLOR)
        else:
            self.image.fill(SHOP_COLOR)
    
    def pick_shop(self):  # a customer picked this shop
        if(self.cash < max_cash):
            self.cash += profit
        else:
            self.counter.update('p')

class Customer(pg.sprite.Sprite):
    def __init__(self, conf: Dict):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface([CUSTOMER_SIZE, CUSTOMER_SIZE])
        self.image.fill(conf['color'])
        self.thresh = random.randint(conf['distance_threshold_min'], conf['distance_threshold_max'])

        self.rect = self.image.get_rect()
        self.rect.x = random.randint(conf['rect_northwest'][0], conf['rect_southeast'][0])
        self.rect.y = random.randint(conf['rect_northwest'][1], conf['rect_southeast'][1])

    def consider_shop(self, shop: Shop, dist):
        if not delivery or dist <= self.thresh:
            shop.pick_shop()
            return True
        return False

def distance(shop: Shop, customer: Customer):
    return math.hypot(abs(shop.rect.x-customer.rect.x), abs(shop.rect.y-customer.rect.y))


if __name__ == "__main__":
    main()
